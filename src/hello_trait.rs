pub trait GenSum<T> {
    fn apply(&self, v: T) -> T;
}

pub struct SumU8 {
    value : u8,
}

impl GenSum<u8> for SumU8 {
    fn apply(&self, v: u8) -> u8 {
        self.value + v
    }
}

pub fn build_s8(v: u8) -> SumU8 {
    SumU8 {
        value : v
    }
}

pub struct SumU16 {
    value : u16,
}

impl GenSum<u16> for SumU16 {
    fn apply(&self, v: u16) -> u16 {
        self.value + v
    }
}

pub fn build_s16(v: u16) -> SumU16 {
    SumU16 {
        value : v
    }
}

pub struct SumString {
    value : String,
}

impl GenSum<String> for SumString {
    fn apply(&self, v: String) -> String {
        let mut owned_string: String = self.value.clone();
        
        owned_string.push_str(v.as_str());

        return owned_string;
    }
}

pub fn build_sstr(v: String) -> SumString {
    SumString {
        value : v.clone()
    }
}

pub fn do_sum<T>(g: &impl GenSum<T>, v: T) -> T {
    g.apply(v)
}
