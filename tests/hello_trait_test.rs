#[cfg(test)]
mod tests {
    #[test]
    fn test_do_sum() {
        let s8 : hello_trait::SumU8 = hello_trait::build_s8(10);
        assert_eq!(hello_trait::do_sum(&s8, 12), 22);
    
        let s16 : hello_trait::SumU16 = hello_trait::build_s16(210);
        assert_eq!(hello_trait::do_sum(&s16, 212), 422);
    
        let sstr : hello_trait::SumString = hello_trait::build_sstr("abc".to_string());
        assert_eq!(hello_trait::do_sum(&sstr, "def".to_string()), "abcdef");
    }
}
