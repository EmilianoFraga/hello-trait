![Rust programmin language logo](https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg)

# Hello Trait
*A project to learn Rust*

## command line

### build
`cargo build`

### test
`cargo test`
